'use strict';

const os = require('os');

class PredefinedJaller {
	constructor() {
	}

	init() {
	}

	load() {
	}

	message(from, msg) {
		if (msg === 'wie geht\'s?') {
			return 'gut, danke';
		} else if (msg === 'ping') {
			return 'pong';
		} else if (msg === 'who am i?') {
			return from;
		} else if (msg.match(/Den wie ?vielten( Tag)? haben wir( heute)?.?/i) ||
			msg.match(/^Welchen( Tag)?haben wir( heute)?.?/i) ||
			msg.match(/^Welcher (Tag )?ist heute.?/i) ||
			msg.match(/^Was für einen Tag haben wir( heute)?.?/i) ||
			msg.match(/^Was ist das( heutige)? Datum( von)?( heute)?.?/i) )
		{
			const d = new Date();
			return d.toDamatchring();
		} else if (msg.match(/Was ist der( heutige)? Wochentag( heute)?.?/i) ||
			msg.match(/^Welchen Wochentag haben wir( heute)?.?/i) ||
			msg.match(/^Welcher Wochentag ist heute.?/i) ||
			msg.match(/^Was für einen Wochentag haben wir( heute)?.?/i) )
		{
			const d = new Date();
			const days = ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'];
			return days[d.getDay()];
		} else if (msg.match(/^((what's )?(your|the) )?cpu.?/i)) {
			return os.cpus()[0].model;
		} else if (msg.match(/^((what's )?(your|the) )?arch(itecture)?.?/i)) {
			return os.arch();
		} else if (msg.match(/^((what's )?(your|the) )?(type|OS|(operating )?system).?/i)) {
			return os.type();
		} else if (msg.match(/^((what's )?(your|the) )?hostname.?/i)) {
			return os.hostname();
		} else if (msg.match(/^((what's )?(your|the) )?(OS('s)? )?platform.?/i)) {
			return os.platform();
		} else if (msg.match(/^((what's )?(your|the) )?(OS('s)? )?(linux )?(kernel( release)?|release).?/i)) {
			return os.release();
		} else if (msg.match(/^((what's )?(your|the) )?(OS('s)? )?load ?(avg|average)?.?/i)) {
			return os.loadavg();
		} else if (msg.match(/^((what's )?(your|the) )?(OS('s)? )?(total)? ?mem(mory)?.?/i)) {
			return ( os.totalmem()/1024/1024 ).toFixed(0) + ' MiB';
		} else if (msg.match(/^((what's )?(your|the) )?(OS('s)? )?free ?mem(mory)?.?/i)) {
			return ( os.freemem()/1024/1024 ).toFixed(0) + ' MiB	 ('+ (os.freemem()/os.totalmem()*100).toFixed(0) + '%)';
		} else if (msg.match(/^((what's )?(your|the) )?(OS('s)? )?up ?time.?/i)) {
			return (os.uptime()/60/60/24).toFixed(1) + ' days';
		}
	}
}

module.exports = PredefinedJaller;
