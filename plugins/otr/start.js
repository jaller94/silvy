'use strict';

const fs = require('fs');
const speakeasy = require('speakeasy');

class OTR {
	constructor() {
		this.conf = null;
	}

	init() {
		this.load();
	}

	load() {
		this.conf = JSON.parse(fs.readFileSync('config/otp.json', 'utf8'));
	}

	save() {
		const json = JSON.stringify(this.conf, null, 2);
		fs.writeFileSync('config/otp.json', json, 'utf8', (err) => {
			if (err) {
				console.error('Saving the plugin OTR configuration failed!');
				console.error(err);
			}
		});
	}

	message(from, msg) {
		const user = from.substr(0, from.indexOf('/'));
		if (user in this.conf) {
			if (msg.match('^hi silvy$')) {
				return 'hi chris';
			} else if (msg.match('^totp')) {
				for (const key in this.conf[user]) {
					if (!this.conf[user].hasOwnProperty(key)) {
						return 'Did not find the key even though it was present.';
					}
					if (msg.endsWith(key)) {
						const seconds = Math.ceil(Date.now()/1000) + 2;
						const secret = this.conf[user][key]['secret'];
						const encoding = this.conf[user][key]['encoding'] || 'base32';
						const token = speakeasy.totp({
							secret: secret,
							encoding: encoding,
							time: seconds // specified in seconds
						});
						return token;
					}
				}
				return 'Key not found!';
			}

			const regex_otpuri = /otpauth:\/\/(htop|totp)\/(.+)%3A(.+)\?secret=(.+)&issuer=(\w+)/;

			if (regex_otpuri.test(msg)) {
				const match = msg.match(regex_otpuri);
				if (match) {
					const entry = {};
					entry.type = match[1];
					entry.product = match[2];
					entry.title = match[3].replace('%40', '@'); //TODO Add functionality of decodeURI
					entry.secret = match[4];
					entry.issuer = match[5]; //TODO Add functionality of decodeURI

					// Update an old entry if it has the same title and issuer.
					const old_entry = this.searchEntry(user, entry.product, entry.title, entry.issuer);
					if (old_entry) {
						old_entry.type = entry.type;
						old_entry.secret = entry.secret;
						old_entry.encoding = entry.encoding;
					} else {
						// No old entry found create a new one.
						this.conf[user]['test'] = entry;
					}

					this.save();

					if (old_entry) {
						return 'Ok, I updated your entry.';
					} else {
						return 'Ok, I saved your new entry. Access it with totp test.';
					}
				}
			}
		}
	}

	searchEntry(user, product, title, issuer) {
		for (const key in this.conf[user]) {
			if (!this.conf[user].hasOwnProperty(key)) {
				throw new Error('Did not find the key even though it was present.');
			}
			const entry = this.conf[user][key];
			console.log(key, entry);
			if (product !== entry.product) continue;
			if (title !== entry.title) continue;
			if (issuer && issuer !== entry.issuer) continue;
			return entry;
		}
	}
}

module.exports = OTR;
