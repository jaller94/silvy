'use strict';

const Client = require('node-xmpp-client');
const fs = require('fs');
const ltx = require('ltx');
const uuid = require('uuid');
const silvy = require('./silvy');

let refreshInterval;

const config = JSON.parse(fs.readFileSync('config/xmpp.json', 'utf8'));

var client = new Client({
  jid: config.user,
  password: config.password
});

let sessionStartTime = null;

// Keepalives
//client.connection.socket.setTimeout(0);
//client.connection.socket.setKeepAlive(true, 10000);
client.connection.reconnect = true;

client.on('stanza', function(stanza) {
  console.log('stanza');
  console.log(stanza);
  console.log('Your stanza: ' + stanza.root().toString());
  if (stanza.is('message') &&
    // Important: never reply to errors!
    (stanza.attrs.type !== 'error')) {
    console.log('Your message: ' + stanza.root().toString());
    const sender = stanza.attrs.from;
    // Direct our message to the sender.
    stanza.attrs.to = stanza.attrs.from;
    delete stanza.attrs.from;
    if (stanza.is('message') && stanza.attrs.type === 'chat' && stanza.getChild('body')) {
      // and reply
      let reply = new ltx.Element('message', {
        to: stanza.attrs.from,
        from: stanza.attrs.to,
        type: 'chat'
      });
      let msg = stanza.getChildText('body');
      stanza.remove('body');
      stanza.c('body').t(silvy(sender, msg));
      //console.log('Sending answer: ' + stanza.root().toString())
      console.log('I wrote: ' + stanza.getChildText('body'));
      client.send(stanza);
    } else {
      // or send back
      //console.log('Sending back: ' + stanza.root().toString());
      //client.send(stanza);
    }
  }
})

client.on('online', function() {
  console.log('Client is online');
  sessionStartTime = new Date();
  client.send(new Client.Stanza('presence', { })
    .c('show').t('chat').up()
    .c('status').t('Happily echoing your <message/> stanzas'));

  silvyInterval();
});

client.on('offline', function () {
  console.log('Client is offline');
});


client.on('connect', function () {
  console.log('Client is connected');
});

client.on('reconnect', function () {
  console.log('Client reconnects …');
});

client.on('disconnect', function (e) {
  if (refreshInterval) {
    clearInterval(refreshInterval);
  }
  if (sessionStartTime) {
    const sessionDuration = (new Date().getTime() - sessionStartTime.getTime()) / 1000;
    console.log('Lost connection after ' + sessionDuration + ' seconds.')
  }
  console.log('Client is disconnected', client.connection.reconnect, e);
});

client.on('error', function(e) {
  console.error(e);
});

process.on('exit', function () {
  client.end();
});

function keepAlive() {
  client.send(new Client.Stanza('iq',
    {
      'xmlns': 'jabber:client',
      'to': client.jid._domain,
      'type': 'get',
      'id': uuid.v1()
    }
  ).c('ping', {'xmlns': 'urn:xmpp:ping'}));
}

function silvyInterval() {
  refreshInterval = setInterval(keepAlive, 60000);
}

setInterval(() => {
  const sessionDuration = (new Date().getTime() - sessionStartTime.getTime()) / 1000;
  console.log('Session age: ' + sessionDuration + ' seconds');
}, 20000);
