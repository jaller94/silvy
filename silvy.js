'use strict';

const plugin_predefined_jaller = require('./plugins/predefined-jaller/start');
const plugin_otr = require('./plugins/otr/start');

const predefined_jaller = new plugin_predefined_jaller();
const otr = new plugin_otr();

predefined_jaller.init();
otr.init();

function addSeparators(str) {
		var parts = (str + "").split("."),
				main = parts[0],
				len = main.length,
				output = "",
				i = len - 1;

		while(i >= 0) {
				output = main.charAt(i) + output;
				if ((len - i) % 3 === 0 && i > 0) {
						output = "." + output;
				}
				--i;
		}
		// put decimal part back
		if (parts.length > 1) {
				output += "," + parts[1];
		}
		return output;
}

function silvy(from, msg) {
	if (msg === null) {
		return 'Hi, you sent me with no content!';
	}

	try {
		const reply =
			predefined_jaller.message(from, msg) ||
			otr.message(from, msg);

		if (reply) {
			return reply;
		}
	} catch (err) {
		console.error('Plugin OTR failed to process a message!');
		console.error(err);
	}

	return msg;
}

module.exports = silvy
